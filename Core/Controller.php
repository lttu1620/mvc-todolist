<?php

namespace Core;

use Exception;

abstract class Controller
{
    protected $routeParam = [];

    public function __construct($routeParam)
    {
        $this->routeParam = $routeParam;
    }

    public function __call($name, $args)
    {
        $method = $name . 'Action';

        if (method_exists($this, $method)) {
            if ($this->before() !== false) {
                call_user_func_array([$this, $method], $args);
                $this->after();
            }
        } else {
            throw new Exception("Method $method not found in controller " . get_class($this));
        }
    }

    protected function before()
    {
    }

    protected function after()
    {
    }

    function redirect(string $endpoint)
    {
        header("Location: /index.php?{$endpoint}");
    }
}
