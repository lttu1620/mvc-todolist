<?php

namespace App\Controllers;

use App\Models\Work;
use Core\Controller;
use Core\View;

class Works extends Controller
{
    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        $works = Work::getAll();

        View::renderTemplate('Works/index.html', [
            'works' => $works
        ]);
    }

    public function updateAction()
    {
        Work::update($_POST);

        $this->redirect('works/index');
    }

    public function createAction()
    {
        Work::create($_POST);

        $this->redirect('works/index');
    }

    public function deleteAction()
    {
        Work::delete($_POST);

        $this->redirect('works/index');
    }

    public function calendarAction()
    {
        $result = [];
        $works = Work::getAll();
        foreach ($works as $work){
            $feed =[];
            $feed['title'] = $work['name'];
            $feed['start'] = $work['start_date'];
            $feed['end'] = $work['end_date'];
            $result[] = $feed;
        }
        echo json_encode($result);
    }
}
