<?php

namespace App\Models;

use Core\Model;
use DateTime;
use PDO;
use PDOException;

class Work extends Model
{
    public static function getAll()
    {
        try {
            $db = static::getDB();

            $stmt = $db->query('
                SELECT id, name, status, start_date, end_date 
                FROM works
                where deleted_at IS NULL
                ORDER BY created_at
            ');
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public static function create(array $param)
    {
        try {
            $db = static::getDB();
            $sql = "INSERT INTO works (name, status, start_date, end_date, created_at, updated_at) 
                VALUES (?,?,?,?,?,?)";

            $now = new DateTime();

            $db->prepare($sql)->execute([
                $param['name'],
                $param['status'],
                $param['start_date'],
                $param['end_date'],
                $now->format('Y-m-d H:i:s'),
                $now->format('Y-m-d H:i:s'),
            ]);

            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }

    public static function update(array $param)
    {
        try {
            $db = static::getDB();
            $sql = "UPDATE works SET name=?, status=?, start_date=?, end_date=? WHERE id=?";
            $db->prepare($sql)->execute([
                $param['name'],
                $param['status'],
                $param['start_date'],
                $param['end_date'],
                $param['id'],
            ]);

            return true;

        } catch (PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }

    public static function delete(array $param)
    {
        try {
            $db = static::getDB();
            $sql = "UPDATE works SET deleted_at=? WHERE id=?";
            $now = new DateTime();
            $db->prepare($sql)->execute([
                $now->format('Y-m-d H:i:s'),
                $param['id'],
            ]);

            return true;

        } catch (PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }
}
